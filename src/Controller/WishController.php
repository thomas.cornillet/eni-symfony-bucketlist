<?php

namespace App\Controller;

use App\Admin\Censurator;
use App\Entity\Wish;
use App\Form\WishType;
use App\Repository\WishRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/wish", name="app_wish_")
 */
class WishController extends AbstractController
{
    /**
     * @Route("/list", name="list")
     */
    public function list(WishRepository $wishRepository): Response
    {
//        $wishes = $wishRepository->findBy([],['dateCreated' => 'DESC']);
        $wishes = $wishRepository->findAllPerso(); // utilisation d'un querybuilder pour réduire le nombre de requêtes pour les jointures
        return $this->render('wish/list.html.twig', [
            "wishes" => $wishes,
        ]);
    }

    /**
     * @Route("/details/{id}", name="details")
     */
    public function index(int $id, WishRepository $wishRepository): Response
    {
        $wish = $wishRepository->find($id);
        if (!$wish) {
            throw $this->createNotFoundException();
        }
        return $this->render('wish/details.html.twig', [
            "wish" => $wish,
        ]);
    }

    /**
     * @Route("/new", name="new")
     */
    public function new(
        Request $request,
        EntityManagerInterface $entityManager,
        WishRepository $wishRepository,
        Censurator $censurator
    ): Response
    {
        $wish = new Wish();
        $wish->setDateCreated(new \DateTime());
        $wish->setIsPublished(true);
        $wish->setAuthor($this->getUser()->getUserIdentifier());
        $wishForm = $this->createForm(WishType::class, $wish);
        $wishForm->handleRequest($request);
        if ($wishForm->isSubmitted() && $wishForm->isValid()) {
            //TODO vérifier s'il y a eu modification, si oui prévenir admin et utilisateur, enregistrer message original, ajouter propriété isAutoCensure
            $purified_title = $censurator->purify($wish->getTitle());
            $wish->setTitle($purified_title);
            $purified_description = $censurator->purify($wish->getDescription());
            $wish->setDescription($purified_description);
            $wishRepository->add($wish, true);
            $this->addFlash('success', 'Idea successfully added!');
            return $this->redirectToRoute('app_wish_details', [
                "id" => $wish->getId()
            ]);
        }
        return $this->render('wish/new.html.twig', [
            'wishForm' => $wishForm->createView(),
        ]);
    }
}
