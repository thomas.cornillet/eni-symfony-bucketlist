<?php

namespace App\Controller;

use App\Entity\Wish;
use App\Form\WishType;
use App\Repository\WishRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="app_admin_")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/moderate/{id}", name="moderate")
     */
    public function moderate(
        int $id,
        WishRepository $wishRepository,
        Request $request,
        EntityManagerInterface $entityManager
    ): Response
    {
        $wish = $wishRepository->find($id);
        // TODO gérer la date de modification et le versionnage
        $wishForm = $this->createForm(WishType::class, $wish);
        $wishForm->handleRequest($request);
        if ($wishForm->isSubmitted() && $wishForm->isValid()) {
//            $entityManager->persist($wish); // pas nécessaire car déjà en base
            $entityManager->flush();
            $this->addFlash('success', 'Idea successfully updated!');
            $id = $wish->getId();
            return $this->redirectToRoute('app_wish_details',[
                "id" => $id
            ]);
        }
        return $this->render('admin/moderate.html.twig', [
            "wish" => $wish,
            'wishForm' => $wishForm->createView(),
        ]);
    }

    /**
     * @Route("/deleteWish/{id}", name="deleteWish", methods={"DELETE"})
     */
    public function deleteDelete(
        Wish $wish,
        WishRepository $wishRepository,
        EntityManagerInterface $entityManager,
        Request $request

    ): Response
    {
        $submittedToken = $request->request->get('token');
        if ($this->isCsrfTokenValid('delete-item', $submittedToken)) {
            $wishRepository->remove($wish, true);
            $this->addFlash('success', 'Idea successfully deleted!');
        }
        else {
            $this->addFlash('error', 'Non valid token !!!'); //TODO vérifier que le message d'erreur s'affiche bien
        }
        return $this->redirectToRoute('app_wish_list');
    }
}
