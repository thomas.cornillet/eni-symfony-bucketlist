<?php

namespace App\Admin;

class Censurator
{
    public const FORBIDEN_WORDS = [
        'BONJOUR'
    ];

    public function purify(string $message): string
    {
        // gérer si le mot est contenu dans une chaine ('forbid' - 'erforbider')
        // voir quoi parcourir en premier pour les performances, ça dépendra surement de la taille des deux tableaux (parcourir d'abord le plus grand)
        $message_tab = explode(' ', $message);
        foreach ($message_tab as $key => $message_part)
        {
            if (in_array(strtoupper($message_part), self::FORBIDEN_WORDS)) {
                $message_tab[$key] = '*';
            }
        }
        return implode(' ', $message_tab);
    }
}