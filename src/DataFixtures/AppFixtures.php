<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\User;
use App\Entity\Wish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Categories
        $categorie1 = new Category();
        $categorie1->setName('Travel & Adventure');
        $manager->persist($categorie1);
        $categorie2 = new Category();
        $categorie2->setName('Sport');
        $manager->persist($categorie2);
        $categorie3 = new Category();
        $categorie3->setName('Entertainment');
        $manager->persist($categorie3);
        $categorie4 = new Category();
        $categorie4->setName('Human Relations');
        $manager->persist($categorie4);
        $categorie5 = new Category();
        $categorie5->setName('Others');
        $manager->persist($categorie5);

        // Users
        $admin = new User();
        $admin->setUsername('admin')
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword('$2y$13$bunXFR1Q0X87hcJYmRkxfudWLdjBULeMdraVZ3z0izidEDjKTzL4m') //adminADMIN
            ->setEmail('admin@admin.bzh');
        $manager->persist($admin);
        $user = new User();
        $user->setUsername('user')
            ->setRoles(["ROLE_USER"])
            ->setPassword('$2y$13$wTzL5viqiNiQgbXvahB6LO01No/OjOuOhrdTYk9iIzbUNcwZ.BLkO') // useruser
            ->setEmail('user@user.bzh');
        $manager->persist($user);

        // wishes
        $categories = [
            1 => $categorie1,
            2 => $categorie2,
            3 => $categorie3,
            4 => $categorie4,
            5 => $categorie5,
        ];
        $users = [
            1 => 'admin',
            2 => 'user'
        ];
        for ($i=1;$i<50;$i++) {
            $wish = new Wish();
            $wish->setTitle('Souhait n° '.$i)
                ->setDescription('Bachi-bouzouk sinapisme zouave jus de réglisse, amiral de bateau-lavoir tête de mule sauvage corsaire cannibale. Patagon soulographe troglodyte phylactère, maraud pignouf cataclysme tonnerre de brest. Mufle sapajou polichinelle froussard. Iconoclaste malotru satrape, coquin athlète complet, olibrius empoisonneur cyrano à quatre pattes apache noix de coco emplâtre. Aérolithe cataplasme hydrocarbure cataclysme, tête de lard kroumir lépidoptère saltimbanque Ku-Klux-Klan. Trafiquant de chair humaine bibendum logarithme. Cyclone vercingétorix de carnaval mouchard renégat. Satrape judas cornichon, pacte à quatre choléra ophicléide.')
                ->setAuthor($users[rand(1,2)])
                ->setIsPublished(true)
                ->setDateCreated(new \DateTime('@'.mt_rand(1, time())))
                ->setCategory($categories[rand(1,5)]);
            $manager->persist($wish);
        }



        $manager->flush();
    }
}
